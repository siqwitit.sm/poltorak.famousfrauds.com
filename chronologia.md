---
layout: chronology
title: Chronologia wydarzeń
ref: chronology
lang: pl
---

{% assign chronology_items = site.data.chronology-pl.items %}

<div class="container-fluid p-0">
  <div class="row">
    <div class="col">
      <div class="container py-2 px-2">
        <div class="row py-md-2 section-header-container">
          <div class="col">
            <header class="section-header">
              <h1 title="{{ page.title | escape }}">{{ page.title | escape }}</h1>
            </header>
          </div>
        </div>

        <div class="row">
          <div class="col">  

            <div class="cd-horizontal-timeline">
            	<div class="timeline">
            		<div class="events-wrapper">
            			<div class="events">
            				<ol>

                      {% for chronology_item in chronology_items %}
                        {% assign chronology_item_date_formatted = chronology_item.date.formatted %}
                        {% assign chronology_item_date_verbal_short = chronology_item.date.verbal.short %}     

                        <li class="event"><a href="#0" data-date="{{ chronology_item_date_formatted }}" {% if chronology_item.selected %}class="selected"{% endif %} >{{ chronology_item_date_verbal_short }}</a></li>

                      {% endfor %}

            				</ol>
            				<span class="filling-line" aria-hidden="true"></span>
            			</div>
            		</div>

            		<ul class="cd-timeline-navigation">
            			<li><a href="#0" class="prev inactive" title="wcześniej">Prev</a></li>
            			<li><a href="#0" class="next" title="później">Next</a></li>
            		</ul>
            	</div>

            	<div class="events-content">
            		<ol>

                  {% for chronology_item in chronology_items %}
                    {% assign chronology_item_title = chronology_item.title %}
                    {% assign chronology_item_date_formatted = chronology_item.date.formatted %}
                    {% assign chronology_item_date_verbal_long = chronology_item.date.verbal.long %}
                    {% assign chronology_item_description = chronology_item.description %}
                    {% assign chronology_item_proofs = chronology_item.proofs %}

                    <li data-date="{{ chronology_item_date_formatted }}" {% if chronology_item.selected %}class="selected"{% endif %} >

                      <h2>{{ chronology_item_title }}</h2>

                      {% comment %}
              				<em>{{ chronology_item_date_verbal_long }}</em>
                      {% endcomment %}                      

                      {{ chronology_item_description }}

                      {% if chronology_item_proofs %}
                        <div class="list-group">
                          <div class="list-group-item list-group-item-primary text-uppercase font-weight-bold">Dokumenty powiązane</div>
                        {% for chronology_item_proof in chronology_item_proofs %}

                          {% assign chronology_item_proof_title = chronology_item_proof.title %}
                          {% assign chronology_item_proof_link = chronology_item_proof.link %}

                          <div class="list-group-item"><a href="{{ chronology_item_proof_link }}" title="{{ chronology_item_proof_title }}" target="_blank">{{ chronology_item_proof_title }}</a></div>

                        {% endfor %}
                        </div>
                      {% endif %}

              			</li>

                  {% endfor %}

            		</ol>
            	</div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</div>

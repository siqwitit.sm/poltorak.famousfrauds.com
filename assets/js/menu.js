
$(function() {

  // mobile menu toggler
  let body = document.querySelector('body')
  let menuTrigger = document.querySelector('#toggle-menu-mobile');
  let menuContainer = document.querySelector('#menu-mobile');
  if (typeof(menuTrigger) != 'undefined' && menuTrigger != null) {
    menuTrigger.onclick = function() {
      menuContainer.classList.toggle('open');
      menuTrigger.classList.toggle('is-active')
      body.classList.toggle('lock-scroll')
    }
  }

});

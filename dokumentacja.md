---
layout: documents
title: Dokumentacja do pobrania
ref: documents
lang: pl
---

<div class="container-fluid p-0">
  <div class="row">
    <div class="col">
      <div class="container py-2 px-2">
        <div class="row py-md-2 section-header-container">
          <div class="col">
            <header class="section-header">
              <h1>{{ page.title | escape }}</h1>
            </header>
          </div>
        </div>
        <div class="row">
          <div class="col mr-10">
            <div class="site-documents-list-container">

              <ul>
              {% comment %}<!-- 1st level -->{% endcomment %}
              {% assign items_1st_level = site.data.documents-pl.items %}
              {% for item_1st_level in items_1st_level %}

                {% assign item_1st_level_type = item_1st_level.type %}
                {% assign item_1st_level_title = item_1st_level.title %}

                {% if item_1st_level.sub-items %}

                  {% if item_1st_level_type == "category" %}
                    <li class="category-level-1 h1">{{ item_1st_level_title }}</li>
                  {% elsif item_1st_level_type == "link" %}
                    <li>{{ item_1st_level_title }}</li>
                  {% endif %}

                  <ul>
                  {% comment %}<!-- 2nd level -->{% endcomment %}
                  {% assign items_2nd_level = item_1st_level.sub-items %}
                  {% for item_2nd_level in items_2nd_level %}

                    {% assign item_2nd_level_type = item_2nd_level.type %}
                    {% assign item_2nd_level_title = item_2nd_level.title %}

                    {% if item_2nd_level.sub-items %}

                      {% if item_2nd_level_type == "category" %}
                        <li class="category-level-2 h2">{{ item_2nd_level_title }}</li>
                      {% elsif item_2nd_level_type == "link" %}
                        <li>{{ item_2nd_level_title }}</li>
                      {% endif %}

                      <ul>

                      {% comment %}<!-- 3rd level -->{% endcomment %}
                      {% assign items_3rd_level = item_2nd_level.sub-items %}
                      {% for item_3rd_level in items_3rd_level %}

                        {% assign item_3rd_level_type = item_3rd_level.type %}
                        {% assign item_3rd_level_title = item_3rd_level.title %}

                        {% if item_3rd_level.sub-items %}

                          {% if item_3rd_level_type == "category" %}
                            <li class="category-level-3 h3">{{ item_3rd_level_title }}</li>
                          {% elsif item_3rd_level_type == "link" %}
                            <li>{{ item_3rd_level_title }}</li>
                          {% endif %}

                          <ul>

                          {% comment %}<!-- 4th level -->{% endcomment %}
                          {% assign items_4th_level = item_3rd_level.sub-items %}
                          {% for item_4th_level in items_4th_level %}

                            {% assign item_4th_level_type = item_4th_level.type %}
                            {% assign item_4th_level_title = item_4th_level.title %}

                            {% if item_4th_level_type == "category" %}
                              <li class="category-level-4 h4">{{ item_4th_level_title }}</li>
                            {% elsif item_4th_level_type == "link" %}
                              {% assign item_4th_level_link = item_4th_level.link %}
                              <li><a href="{{ item_4th_level_link }}" title="{{ item_4th_level_title }}" target="_blank">{{ item_4th_level_title }}</a></li>
                            {% endif %}
                          {% endfor %}
                          </ul>

                        {% elsif item_3rd_level.link %}
                          {% assign item_3rd_level_link = item_3rd_level.link %}
                          <li><a href="{{ item_3rd_level_link }}" title="{{ item_3rd_level_title }}" target="_blank">{{ item_3rd_level_title }}</a></li>
                        {% endif %}
                      {% endfor %}
                      </ul>

                    {% elsif item_2nd_level.link %}
                      {% assign item_2nd_level_link = item_2nd_level.link %}
                      <li><a href="{{ item_2nd_level_link }}" title="{{ item_2nd_level_title }}" target="_blank">{{ item_2nd_level_title }}</a></li>
                    {% endif %}
                  {% endfor %}
                  </ul>

                {% elsif item_1st_level.link %}
                  {% assign item_1st_level_link = item_1st_level.link %}
                  <li><a href="{{ item_1st_level_link }}" title="{{ item_1st_level_title }}" target="_blank">{{ item_1st_level_title }}</a></li>
                {% endif %}
              {% endfor %}
              </ul>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

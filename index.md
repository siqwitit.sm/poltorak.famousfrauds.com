---
layout: home
ref: index
lang: pl
---

<div class="container-fluid p-0">
 <div class="row">
  <div class="col">
   <div class="container site-home-intro-container py-2 px-2">

    <div class="site-articles-list-container w-75">
     {% assign articles = site.data.articles | where:"language", page.lang | sort: 'date' | reverse %}
     {% for article in articles %}
       {% assign article_id = article.date | date: date_format %}
       <div class="site-article-container pb-1" id="{{ article_id }}">
         <div class="site-article-title pb-1">
           <h3>{{ article.title | escape }}</h3>
         </div>         
         {% if article.embed_url %}
         <div class="site-article-embed-container p-1">
           {% linkpreview article.embed_url %}
         </div>
         {% endif %}         
         <div class="site-article-body pt-1">
         {% if article.embed_url %}
           {{ article.comment }}
         {% elsif article.local_url %}
           {% include localurl.html url=article.local_url description=article.comment %}
         {% endif %}
         </div>
       </div>
       {% if forloop.last == false %}<hr/>{% endif %}
     {% endfor %}
    </div>

   </div>
  </div>
 </div>

</div>
